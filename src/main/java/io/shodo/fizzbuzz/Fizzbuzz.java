package io.shodo.fizzbuzz;

public class Fizzbuzz {
  private String result = "";

  private boolean isFizz(int n) {
    return n != 0 && n % 3 == 0;
  }

  private boolean isBuzz(int n) {
    return n != 0 && n % 5 == 0;
  }

  private boolean thenDisplayFizz() {
    result = "Fizz";
    return true;
  }

  private boolean thenDisplayBuzz() {
    result += "Buzz";
    return true;
  }

  private boolean orElse(int n) {
    result = String.valueOf(n);
    return true;
  }

  private boolean build(int value) {
    return (isFizz(value) && thenDisplayFizz())
        | (isBuzz(value) && thenDisplayBuzz())
        || orElse(value);
  }

  public String evaluate(int value) {
    result = "";
    build(value);
    return result;
  }
}
