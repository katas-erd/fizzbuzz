## Fizzbuzz

The description of Fizzbuzz is [here](https://codingdojo.org/kata/FizzBuzz/)

This repository shows how to solve the fizzbuzz katas with different manners and practices

There is too much over-engineering in this code. But it provides a good illustration of how the *things* work.

* [With no control structures (no conditions, no loops, etc.)](https://gitlab.com/romha-katas/fizzbuzz/-/tree/no-control-structure)
* [Monoid & open/closed](https://gitlab.com/romha-katas/fizzbuzz/-/tree/monoid)
